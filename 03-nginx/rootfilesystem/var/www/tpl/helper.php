<?php


$fix=<<<AAA
</head>
<body>
<script type="text/javascript" charset="utf-8">
    function loadScript(url, callback){
        var script = document.createElement ("script")
        script.type = "text/javascript";
        if (script.readyState){
            script.onreadystatechange = function(){
                if (script.readyState == "loaded" || script.readyState == "complete"){
                    script.onreadystatechange = null;
                    callback();
                }
            };
        } else {
            script.onload = function(){
                callback();
            };
        }
        script.src = url;
        document.getElementsByTagName("head")[0].appendChild(script);
    }
    setTimeout(function(){
        loadScript('/og.js?_t='+ (new Date()).getTime(),function(){});
    },200);
</script>
</body>
</html>
AAA;

$files = glob('*.html',GLOB_MARK);
$path=__DIR__."/";
foreach($files as $file){
    $raw=$path.$file;
    $out=$path.str_replace('.html','_tb.html',$file);
    $data=file_get_contents($raw);
    $want=array_slice(explode("\n",$data),0,17);
    file_put_contents($out,implode("\n",$want)."\n".$fix);
}

// $files = glob('*.html',GLOB_MARK);
// $path=__DIR__."/";
// foreach($files as $file){
//     if(strpos($file,'_tb')){
//         unlink($path.$file);
//     }
// }