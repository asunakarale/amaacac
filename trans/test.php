<?php
require 'vendor/autoload.php';

use Google\Cloud\Translate\V3\TranslationServiceClient;

$translationServiceClient = new TranslationServiceClient();

$data=array_filter(explode("\n",file_get_contents('./str.txt')));
$data = array_filter($data);
$all_data = [];
foreach ($data as $key=>$datum){
    $all_data['str_' . ($key + 1)] = $datum;
}
/** Uncomment and populate these variables in your code */

 $targetLanguage = 'zh-tw';
 $projectId = 'secret-medium-309009';
$formattedParent = $translationServiceClient->locationName($projectId, 'global');

$langs = ["af", "sq", "am", "ar", "hy", "az", "eu", "be", "bn", "bs", "bg", "ca", "co", "hr", "cs", "da", "nl", "eo", "et", "fi", "fr", "fy", "gl", "ka", "de", "el", "gu", "ht", "ha", "iw", "hi", "hu", "is", "ig", "id", "ga", "it", "ja", "jv", "kn", "kk", "km", "rw", "ko", "ku", "ky", "lo", "la", "lv", "lt", "lb", "mk", "mg", "ms", "ml", "mt", "mi", "mr", "mn", "my", "ne", "no", "ny", "or", "ps", "fa", "pl", "pt", "pa", "ro", "ru", "sm", "gd", "sr", "st", "sn", "sd", "si", "sk", "sl", "so", "es", "su", "sw", "sv", "tl", "tg", "ta", "tt", "te", "th", "tr", "tk", "uk", "ur", "ug", "uz", "vi", "cy", "xh", "yi", "yo", "zh",'zh-tw'];
//$langs = ["af"];

$trans_lang = include('./lang.php');


foreach($langs as $lang){
    $waiting_trans = $all_data;
    if (isset($trans_lang[$lang])){
        $waiting_trans = array_diff_key($all_data,$trans_lang[$lang]);
    }

    if (count($waiting_trans)>0){
        echo sprintf("语言包：%s ,将要更新 %d 条翻译\n",$lang,count($waiting_trans));
    }else{
        echo sprintf("语言包：%s ,无需要更新的翻译，本次跳过\n",$lang);
        continue;
    }
    try {
        $response = $translationServiceClient->translateText(
            $waiting_trans,
            $lang,
            $formattedParent
        );
        $waiting_key = array_keys($waiting_trans);
        // Display the translation for each input text provided
        foreach ($response->getTranslations() as $key=>$translation) {
            $trans_lang[$lang][$waiting_key[$key]] = $translation->getTranslatedText();
        }
    } finally {
        $translationServiceClient->close();
    }
}
$trans_lang['en'] = $all_data;

file_put_contents('./lang.php', "<?php \n return " . var_export($trans_lang, true).';');
